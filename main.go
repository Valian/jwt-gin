package main

import (
	"auth/config"
	"auth/routes"
)

var identityKey = "id"

func main() {
	config.ConnectDataBase()

	r := routes.Routes()
	_ = r.Run()
}
package config

import (
	"auth/models"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)
const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "1346"
	dbname   = "gorm"
)
/*
const (
	host     = "ec2-35-172-73-125.compute-1.amazonaws.com"
	port     = 5432
	user     = "zxaesyjblfuogt"
	password = "bf80be2a90e6e9a676451b507fd8b7df8349f2c094527b6da9d9baf7873f7549"
	dbname   = "d8764sj6q4a3pu"
)
 */

var DB *gorm.DB

func ConnectDataBase() {
	connect := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	database, err := gorm.Open("postgres", connect)

	if err != nil {
		panic("Failed to connect to database!")
	}


	database.AutoMigrate(&models.User{})
	DB = database
}

package controllers

import (
	"auth/config"
	"auth/models"
	"auth/utils"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/gin-gonic/gin"
	_ "github.com/swaggo/swag/example/celler/httputil"
	_ "golang.org/x/crypto/bcrypt"
	"net/http"
)

type UpdateUserInput struct {
	Nombre  string `json:"nombre"`
	Email string `json:"email"`
	Password string `json:"password"`
}

type CreateUserInput struct {
	Nombre  string `json:"nombre" binding:"required"`
	Email string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// @tags CRUD usuarios
// @Summary Listado de usuarios
// @Description get string by ID
// @ID get-all-users
// @Accept  json
// @Produce  json
// @Success 200 {object} models.User
// @Header 200 {string} Token "qwerty"
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /users [get]
func FindUsers(c *gin.Context) {
	var users []models.User
	config.DB.Find(&users)

	c.JSON(http.StatusOK, gin.H{"data": users})
}

// @tags CRUD usuarios
// @Summary Crear un usuario
// @Description get string by ID
// @ID post-new-user
// @Accept  json
// @Produce  json
// @Param id path int true "Account ID"
// @Success 200 {object} models.User
// @Header 200 {string} Token "qwerty"
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /users/{id} [post]
func CreateUser(c *gin.Context) {
	// Validate input
	var input CreateUserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	pass, _ := utils.HashPassword(input.Password)
	// Create book
	user := models.User{Nombre: input.Nombre,Email: input.Email,Password: pass}
	config.DB.Create(&user)

	c.JSON(http.StatusOK, gin.H{"data": user})
}
// @tags CRUD usuarios
// @Summary Mostrar detalles de un usuario
// @Description get string by ID
// @ID get-one-user
// @Accept  json
// @Produce  json
// @Param id path int true "Account ID"
// @Success 200 {object} models.User
// @Header 200 {string} Token "qwerty"
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /users/{id} [get]
func FindUser(c *gin.Context) {  // Get model if exist
	var user models.User

	if err := config.DB.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": user})
}

// @tags CRUD usuarios
// @Summary editar usuario
// @Description get string by ID
// @ID patch-user
// @Accept  json
// @Produce  json
// @Param id path int true "Account ID"
// @Success 200 {object} models.User
// @Header 200 {string} Token "qwerty"
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /users/{id} [patch]
func UpdateUser(c *gin.Context) {
	// Get model if exist
	var user models.User
	if err := config.DB.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input UpdateUserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	config.DB.Model(&user).Updates(input)

	c.JSON(http.StatusOK, gin.H{"data": user})
}
// @tags CRUD usuarios
// @Summary Eliminar usuario
// @Description get string by ID
// @ID delete-user
// @Accept  json
// @Produce  json
// @Param id path int true "Account ID"
// @Success 200 {object} models.User
// @Header 200 {string} Token "qwerty"
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /users/{id} [delete]
func DeleteUser(c *gin.Context) {
	// Get model if exist
	var user models.User
	if err := config.DB.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	config.DB.Delete(&user)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
func UserLogin(password string, nombre string)bool {  // Get model if exist
	var user models.User

	if err := config.DB.Where("nombre = ?", nombre).First(&user).Error; err != nil {}
	fmt.Println(user)
	if utils.CheckPasswordHash(password, user.Password) {
		return true
	}else {
		return false
	}
}
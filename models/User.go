package models
import (
	_ "github.com/jinzhu/gorm"
)


type User struct {
	ID     uint   `json:"id" gorm:"primary_key"`
	Nombre  string `json:"nombre"`
	Email  string `json:"email"`
	Password string `json:"password"`
}
